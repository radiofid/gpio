#######################################################################
#
#  (C) Copyright 2004 Roman Avramenko <roman@imsystems.ru>
#  GPIO Makefile
#
#######################################################################

APP = gpio

CROSS_COMPILE=/opt/arm/bin/arm-linux-
ARCH=arm

TRGPATH = ../..
LIBS = 

INSTALL = $(TRGPATH)/install
INCLUDE = .

CC = $(CROSS_COMPILE)gcc
STRIP=$(CROSS_COMPILE)strip
CFLAGS = -DLINUX
LDFLAGS = -lc

SRCPATH = $(TRGPATH)/src/$(APP)
SRC := $(APP).c
OBJ := $(APP).o

.c.o:
	$(CC) $(CFLAGS) -I$(INCLUDE) -c -o $@ $<

all: $(APP)

$(APP):	$(OBJ)
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS) $(LIBS)

install:
	cp $(APP) $(INSTALL)

clean:
	rm -f *.o *.gdb $(APP)

     
